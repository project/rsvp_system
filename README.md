# RSVP System Cookbook

Welcome to the RSVP System, a project for building reservation systems on Drupal. 

## Overview

The goal of the RSVP System project is to provide a flexible solution for managing various types of reservations. Use cases include room reservations, equipment loans, and other RSVP-type applications. This project can be used to build build advance calendar functionalities; helping to track many aspects of a quality reservations and bookings platfom. Best of all, because it's Drupal, you can adjust as needed. Enjoy.

## Documentation

Project documentation is a real-time reflection of the latest changes in the head branch of the Recipe project repository. To ensure that our documentation is always up-to-date, we utilize a continuous integration pipeline that automates the conversion and deployment process.

### Pipeline Process

- Markdown to HTML Conversion: We run a CLI job within our pipeline that converts the project's Markdown (.md) files to HTML format.

- Deployment: The converted HTML files are then deployed and served via GitLab Pages, providing you with the most current and accurate information.

This automated process ensures that any updates or additions to the documentation in the head branch are immediately reflected on **[RSVP-System.org](https://rsvp-system.org)**.

## Features

- Easy to use and customizable
- Supports multiple types of reservations
- Integrated calendar functionalities
- Built on Drupal, leveraging its powerful features and ecosystem

## Getting Started

To get started with the RSVP System, please visit our [Getting Started Guide](https://gitlab.com/rsvp-system/rsvp-system/-/blob/main/docs/index.md?ref_type=heads).

## Contributing

We welcome contributions to the RSVP System. If you're interested in contributing, please read our [Contributing Guidelines](https://gitlab.com/rsvp-system/rsvp-system/-/blob/main/docs/index.md?ref_type=heads).

## License

The RSVP System is open-source software licensed under the [GPL 2.0 License](https://gitlab.com/rsvp-system/rsvp-system/-/blob/main/LICENSE.md?ref_type=heads).

## Contact

If you have any questions or need further assistance, please contact us at support@rsvp-system.org or find us on [Drupal Slack](https://drupal.slack.com) in the `#rsvp-system` channel.

Thank you for using the RSVP System!
