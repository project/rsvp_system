# RSVP System Modules
The RSVP System Module is comprised of a collection of these smaller sub-modules:

**RSVP Department:**
A Taxonomy Term used to categorize locations. Allows for sorting and filtering of locations by department.

**RSVP Location:**
A Content Type designed for managing detailed information about reservable locations. Belongs to a Department. Can be referenced from RSVP Resources.

**RSVP Resource:**
A Content Type designed for managing detailed information about reservable resources.

**RSVP Request:**
A Content Type for submitting reservation details. Has a Data Retention Desturcution Date. Used for Room Reservation and Equipmenr Loan requests.

**RSVP Event:**
A Content Type for Events which are highly promoted on the system. Belongs to a RSVP Department. Can be set in conjunction with a RSVP Request. (A Room Reservation and Program Event can be combined.)

**RSVP Shift:**
A Content for team shifts and schedules. (RSVP Helpdesk)

**RSVP Log:** A Content Type for logging all activities related to reservation requests.
Includes recording outcomes like approvals, declines, and cancellations.
Serves as a historical record to maintain data integrity and provide insights into reservation trends and outcomes.

** RSVP Import:**
An optional module to support "Feeds" import configurations. Field mappings are made using sample CSV files from the RSVP Recipe cookbook.


#### RSVP System Modules:
rsvp_system.info.yml
name: 'RSVP System'
type: module
description: 'Main modules for the RSVP System.'
package: RSVP System
dependencies:
 - rsvp_event
 - rsvp_log
 - rsvp_resource
 - **rsvp_department** // installs, testing
 - **rsvp_location** // installs, testing
 - rsvp_request 
 - rsvp_shift
 - rsvp_import

### Department Taxonomy Fields
 - field_rsvp_short_name
 - field_rsvp_full_name 
 - field_rsvp_logo

## Location Content Type Fields
 - field_rsvp_department	
 - field_rsvp_short_name
 - field_rsvp_full_name 
 - field_rsvp_logo
