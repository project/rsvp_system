# RSVP System
An Open Source Drupal project focused on building advance reservation systems.

## RSVP System / [Recipe Cookbook](https://gitlab.com/rsvp-system/rsvp-system)
A Composer project that holds the RSVP System Recipes, ECA workflow models, example data and documentation. Recipes are applied to provide site configurations and sample content.

**Provided by the RSVP System Recipe:**

 - **Workflow Models**: ECA models for form validation, moderation, content tagging and data retention.
 - **This Documentation**: Doucmentation, stored in Markdown format, is managed within the recipe project. Changes to the documentation repository are convered from Markdown (.md) to HTML and served via a Gitlab build process. Documentation is updated real-time as commits are pushed into the repository.

## RSVP System / [Drupal Module](https://gitlab.com/rsvp-system/rsvp-system-module)
A Composer project that holds the RSVP System Module. This Drupal contributed module is installed by the RSVP System Recipe.

**Provided by the RSVP System Module:**

- **RSVP Department**: A taxonomy used to categorize locations. This taxonomy allows for sorting and filtering of locations by department.

- **RSVP Location**: A content type designed for managing detailed information about reservable locations.

- **RSVP Resource**: A content type designed for managing detailed information about reservable resources.

- **RSVP Request**: A content type used for submitting reservation details.

- **RSVP Log**: A content type for logging all activities related to reservation requests. This includes recording outcomes like approvals, declines, and cancellations, serving as a historical record to maintain data integrity and provide insights into reservation trends and outcomes.

- **Feed Imports**: Importers for RSVP Departments (Taxonomy), RSVP Locations (Content)
